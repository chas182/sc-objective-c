//
//  main.m
//  FruitBasket
//
//  Created by fpmi on 10.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Apple.h"
#import "Orange.h"
#import "Basket.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Apple *apple = [[Apple alloc] init];
        Apple *apple1 = [[Apple alloc] init];
        Apple *apple2 = [[Apple alloc] init];
        Orange *orange = [[Orange alloc] init];
        
        Basket *basket = [[Basket alloc] init];
        [basket putFruit:apple];
        [basket putFruit:apple1];
        [basket putFruit:apple2];
        [basket putFruit:orange];
        
        Iterator *it = [basket getIterator];
        
        while ([it hasNext]) {
            NSLog(@"%@", [(Fruit*)[it next] getInfo]);
        }
    }
    return 0;
}
