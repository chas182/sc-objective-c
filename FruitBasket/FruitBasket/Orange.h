//
//  Orange.h
//  FruitBasket
//
//  Created by Admin on 16.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fruit.h"

@interface Orange : Fruit

@end

@implementation Orange

- (NSString*) getInfo {
    return @"This is an orange";
}

@end