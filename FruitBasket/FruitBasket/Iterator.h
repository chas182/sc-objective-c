//
//  Iterator.h
//  FruitBasket
//
//  Created by Admin on 16.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Iterator : NSObject

-(NSObject*) next;
-(BOOL) hasNext;

@end

@implementation Iterator : NSObject

-(NSObject*) next {
    return nil;
}
-(BOOL) hasNext {
    return false;
}

@end