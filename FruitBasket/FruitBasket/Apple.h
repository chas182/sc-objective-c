//
//  Apple.h
//  FruitBasket
//
//  Created by fpmi on 10.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fruit.h"

@interface Apple : Fruit

@end

@implementation Apple

- (NSString*) getInfo {
    return @"This is an apple";
}

@end
