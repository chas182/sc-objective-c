//
//  Fruit.h
//  FruitBasket
//
//  Created by fpmi on 10.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Fruit : NSObject

-(NSString*) getInfo;

@end

@implementation Fruit : NSObject

-(NSString*) getInfo {
    return @"";
}

@end
