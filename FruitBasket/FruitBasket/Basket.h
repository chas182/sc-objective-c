//
//  Basket.h
//  FruitBasket
//
//  Created by fpmi on 10.09.15.
//  Copyright (c) 2015 fpmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Iterator.h"
#import "Fruit.h"

@interface Basket : NSObject

-(id)init;
-(Iterator*) getIterator;
-(void) putFruit: (Fruit*) fruit;

@end


@interface __BasketIterator : Iterator

@property int index;

-(id) init;
@end


@implementation Basket

NSMutableArray *fruits;

-(id)init {
    self = [super init];
    fruits = [[NSMutableArray alloc] init];
    return self;
}

-(void) putFruit:(Fruit *)fruit {
    [fruits addObject: fruit];
}

-(Iterator*) getIterator {
    return [[__BasketIterator alloc] init];
}

@end



@implementation __BasketIterator

-(id) init {
    self = [super init];
    self.index = -1;
    return self;
}

-(NSObject*) next {
    return [fruits objectAtIndex: ++self.index];
}
-(BOOL) hasNext {
    return ((unsigned long) [fruits count]) > self.index + 1;
}

@end